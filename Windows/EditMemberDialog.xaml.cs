﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using AForge.Video;
using AForge.Video.DirectShow;
using gym_manager.Model;
using gym_manager.Service;

namespace gym_manager.Windows
{
    /// <summary>
    /// Interaction logic for NewMemberDialog.xaml
    /// </summary>
    public partial class EditMemberDialog : Window
    {
        private readonly MemberService service;
        private Member member;
        private FilterInfo VideoDevice { get; set; }
        private IVideoSource VideoSource;
        private ImageSource capturedImate { get; set; }

        public EditMemberDialog(int id)
        {

            InitializeComponent();
            GetVideoDevices();
            Closing += Window_Closing;
            service = new MemberService();
            member = service.FindById(id);

            BitmapImage image = new BitmapImage();
            MemoryStream ms = new MemoryStream(member.Image);
            image.BeginInit();
            image.StreamSource = ms;
            image.EndInit();
            this.ImageCapture.Source = image;
            this.Name.Text = member.Name;
            this.Phone.Text = member.PhoneNumber;
            this.Remark.Text = member.Remark;
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            StopCamera();
        }

        private void Capture_Click(object sender, RoutedEventArgs e)
        {
            ImageWebCam.Opacity = 0.2;
            ImageCapture.Source = ImageWebCam.Source;
            capturedImate = ImageWebCam.Source;
            StopCamera();
            ImageCapture.Visibility = Visibility.Visible;
            ImageWebCam.Visibility = Visibility.Hidden;
            ImageCapture.Opacity = 1;
            ButtonCapture.IsEnabled = false;
            IgnoreCapture.IsEnabled = false;
        }

        private void Cancel_Capture_Click(object sender, RoutedEventArgs e)
        {
            CancelCapture();
        }

        private void Ignore_Capture_Click(object sender, RoutedEventArgs e)
        {
            BitmapImage dummy = new BitmapImage();
            dummy.BeginInit();
            dummy.UriSource = new Uri(@"../dummy.png", UriKind.Relative);
            dummy.EndInit();
            ImageCapture.Source = dummy;
            capturedImate = dummy;

            StopCamera();
            ImageCapture.Visibility = Visibility.Visible;
            ImageWebCam.Visibility = Visibility.Hidden;
            ImageCapture.Opacity = 1;
            ButtonCapture.IsEnabled = false;
        }


        private void CancelCapture()
        {
            ImageCapture.Opacity = 0.2;
            StartCamera();
            ImageCapture.Source = null;
            ImageCapture.Visibility = Visibility.Hidden;
            ImageWebCam.Visibility = Visibility.Visible;
            ImageWebCam.Opacity = 1;
            ButtonCapture.IsEnabled = true;
            IgnoreCapture.IsEnabled = true;
            capturedImate = null;
        }

        private void video_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            try
            {
                BitmapImage bi;
                using (var bitmap = (Bitmap)eventArgs.Frame.Clone())
                {
                    bi = bitmap.ToBitmapImage();
                }
                bi.Freeze(); // avoid cross thread operations and prevents leaks
                Dispatcher.BeginInvoke(new ThreadStart(delegate { ImageWebCam.Source = bi; }));
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error on _videoSource_NewFrame:\n" + exc.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                StopCamera();
            }
        }

        private void StartCamera()
        {
            if (VideoDevice != null)
            {
                VideoSource = new VideoCaptureDevice(VideoDevice.MonikerString);
                VideoSource.NewFrame += video_NewFrame;
                VideoSource.Start();
            }
        }

        private void StopCamera()
        {
            if (VideoSource != null && VideoSource.IsRunning)
            {
                VideoSource.SignalToStop();
                VideoSource.NewFrame -= (video_NewFrame);
            }
        }

        private void GetVideoDevices()
        {
            ObservableCollection<FilterInfo> videoDevices = new ObservableCollection<FilterInfo>();
            foreach (FilterInfo filterInfo in new FilterInfoCollection(FilterCategory.VideoInputDevice))
            {
                videoDevices.Add(filterInfo);
            }
            if (videoDevices.Any())
            {
                VideoDevice = videoDevices[0];
            }
            else
            {
                MessageBox.Show("No video sources found", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {

            if (string.IsNullOrEmpty(Name.Text))
            {
                MessageBox.Show(this, "Điền tên vào đã em !", "Lỗi rồi");
                return;
            }

            //if (string.IsNullOrEmpty(Phone.Text))
            //{
            //    MessageBox.Show(this, "Số điện thoại đâu em ?", "Lỗi rồi");
            //    return;
            //}

            if (capturedImate == null)
            {
                MessageBox.Show(this, "Chưa chụp hình em ơi !", "Lỗi rồi");
                return;
            }

            member.Name = Name.Text;
            member.PhoneNumber = Phone.Text;
            member.Remark = Remark.Text;
            member.Image = ImageSourceToBytes(capturedImate);
            service.Insert(member);

            Close();

            ((Windows.Home) Application.Current.MainWindow).UpdateMember(member);
            ((Windows.Home) Application.Current.MainWindow).UpdateInfo();
        }

        public byte[] ImageSourceToBytes(ImageSource imageSource)
        {
            BitmapEncoder encoder = new PngBitmapEncoder();
            byte[] bytes = null;

            if (imageSource is BitmapSource bitmapSource)
            {
                encoder.Frames.Add(BitmapFrame.Create(bitmapSource));

                using (var stream = new MemoryStream())
                {
                    encoder.Save(stream);
                    bytes = stream.ToArray();
                }
            }

            return bytes;
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}