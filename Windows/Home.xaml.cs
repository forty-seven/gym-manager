﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using gym_manager.Model;
using gym_manager.Service;
using Button = System.Windows.Controls.Button;
using MessageBox = System.Windows.MessageBox;

namespace gym_manager.Windows
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : Window
    {
        private readonly MemberService service = new MemberService();
        private readonly DashboardService dashboardService = new DashboardService();

        public Home()
        {
            InitializeComponent();
            ICollection<Member> expiredMembers = service.GetExpiredMember(7);
            this.expiredMember.ItemsSource = expiredMembers;
            //this.members.ItemsSource = expiredMembers;
            this.NumberOfDate.Text = "7";
            UpdateInfo();
        }

        public void UpdateMember(Member member)
        {
            this.members.ItemsSource = new List<Member> { member };
        }

        public void UpdateInfo()
        {
            NumberOfRegistration.Content = dashboardService.GetNumberOfRegisterByMonth(DateTime.Now.Month);
            Money.Content = $"{dashboardService.GeMoneyByMonth(DateTime.Now):n0}";
        }

        private void NewMember_Click(object sender, RoutedEventArgs e)
        {
            var addNewDialog = new NewMemberDialog { Owner = this };
            addNewDialog.ShowDialog();
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
            this.members.ItemsSource = service.Search(SearchInput.Text);
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void NumberOfDate_TextChanged(object sender, TextChangedEventArgs e)
        {
            var isNumeric = int.TryParse(NumberOfDate.Text, out int number);
            this.expiredMember.ItemsSource = isNumeric ? service.GetExpiredMember(number) : null;
        }

        private void Extend_Click(object sender, RoutedEventArgs e)
        {
            var id = ((Button)sender).Tag;
            var extendMember = new ExtendMember(int.Parse(id.ToString())) { Owner = this };
            extendMember.ShowDialog();
        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            var id = ((Button)sender).Tag;
            var editMember = new EditMemberDialog(int.Parse(id.ToString())) { Owner = this };
            editMember.ShowDialog();
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            var id = ((Button)sender).Tag;
            var member = service.FindById((int)id);
            MessageBoxResult dr = MessageBox.Show($"Chuẩn bị xóa {member.Name}", "Xóa thành viên", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (dr == MessageBoxResult.Yes)
            {

                service.Delete((int)id);
                this.members.ItemsSource = service.Search(SearchInput.Text);
                UpdateInfo();
            }

        }
    }
}