﻿using System;
using System.Linq;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using gym_manager.Model;
using gym_manager.Service;
using Newtonsoft.Json;

namespace gym_manager.Windows
{
    /// <summary>
    /// Interaction logic for ExtendMember.xaml
    /// </summary>
    public partial class ExtendMember : Window
    {
        private readonly MemberService service = new MemberService();
        private string selectedPrice;
        private Member member;

        public ExtendMember(int id)
        {
            InitializeComponent();
            member = service.FindById(id);
            BitmapImage image = new BitmapImage();
            MemoryStream ms = new MemoryStream(member.Image);
            image.BeginInit();
            image.StreamSource = ms;
            image.EndInit();
            this.Image.Source = image;
            this.Name.Content = member.Name;
            this.Phone.Content = member.PhoneNumber;
            this.Remark.Content = member.Remark;
            this.History.Text = member.RegistrationHistoryDisplay;
            ExtendDate.SelectedDate = DateTime.Now;
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            selectedPrice = sender is RadioButton radio ? radio.Content.ToString().Replace(".", "") : "180000";
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Extend_Click(object sender, RoutedEventArgs e)
        {

            // Validate
            DateTime extendDate = ExtendDate.SelectedDate ?? DateTime.Now;
            if (member.LastExpiredDate >= extendDate)
            {
                MessageBox.Show(this, $"Em này ngày {member.LastExpiredDate:dd-MM-yyyy} mới hết hạn, gia hạn {extendDate:dd-MM-yyyy} không được");
                return;
            }

            Registration extend = new Registration
            {
                Price = int.Parse(selectedPrice),
                RegistrationDate = ExtendDate.SelectedDate ?? DateTime.Now
            };
            
            member.RegistrationHistory.Add(extend);
            member.LastRegistrationDate = extendDate;
            member.LastRegistrationMoney = int.Parse(selectedPrice);
            member.LastExpiredDate = extendDate.AddMonths(1);
            service.Extend(member);

            Close();

            ((Windows.Home)Application.Current.MainWindow).UpdateMember(member);
            ((Windows.Home)Application.Current.MainWindow).UpdateInfo();
        }
    }
}
