﻿using gym_manager.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace gym_manager.Service
{
    internal class MemberService
    {
        public void Insert(Member member)
        {
            using (GYM_MANAGEREntities context = new GYM_MANAGEREntities())
            {
                MEMBER entity = new MEMBER
                {
                    NAME = member.Name,
                    PHONE = !string.IsNullOrEmpty(member.PhoneNumber) ? Decimal.Parse(member.PhoneNumber) : (decimal?)null,
                    IMAGE = member.Image,
                    REMARK = member.Remark,
                    REGISTRATION_HISTORY = JsonConvert.SerializeObject(member.RegistrationHistory),
                    LAST_REGISTRATION = member.LastRegistrationDate,
                    LAST_REGISTRATION_MONEY = member.LastRegistrationMoney,
                    LAST_EXPIRED_DATE = member.LastExpiredDate,
                    CREATE_DATE = DateTime.Now,
                    DELETED = false
                };

                context.MEMBERs.Add(entity);
                context.SaveChanges();
            }
        }

        public List<Member> GetExpiredMember(int days)
        {
            List<Member> members;
            DateTime now = DateTime.Now;
            var expectDate = now.Date.AddDays(-days);
            using (GYM_MANAGEREntities context = new GYM_MANAGEREntities())
            {
                IQueryable<Member> query = from m in context.MEMBERs
                                           where m.LAST_EXPIRED_DATE > expectDate && m.LAST_EXPIRED_DATE < now.Date
                                           select new Member
                                           {
                                               Id = m.ID,
                                               Name = m.NAME,
                                               Image = m.IMAGE,
                                               PhoneNumber = m.PHONE.ToString(),
                                               Remark = m.REMARK,
                                               RegistrationHistoryString = m.REGISTRATION_HISTORY,
                                               LastRegistrationDate = m.LAST_REGISTRATION.Value,
                                               LastRegistrationMoney = m.LAST_REGISTRATION_MONEY.Value,
                                               LastExpiredDate = m.LAST_EXPIRED_DATE.Value,
                                               CreateDate = m.CREATE_DATE.Value,
                                               UpdateDate = m.UPDATE_DATE
                                           };
                members = query.ToList();
            }

            foreach (Member m in members)
            {
                m.RegistrationHistory = JsonConvert.DeserializeObject<List<Registration>>(m.RegistrationHistoryString);
            }

            return members;
        }

        public void Delete(int id)
        {
            using (var context = new GYM_MANAGEREntities())
            {
                var memberEntity = context.MEMBERs.FirstOrDefault(x => x.ID == id);

                if (memberEntity == null) return;
                {
                    memberEntity.DELETED = true;
                    memberEntity.UPDATE_DATE = DateTime.Now;

                    context.MEMBERs.Attach(memberEntity);
                    var entry = context.Entry(memberEntity);
                    entry.Property(x => x.DELETED).IsModified = true;
                    entry.Property(x => x.UPDATE_DATE).IsModified = true;
                    context.SaveChanges();
                }
            }
        }

        public void Update(Member member)
        {
            using (GYM_MANAGEREntities context = new GYM_MANAGEREntities())
            {
                var memberEntity = context.MEMBERs.FirstOrDefault(x => x.ID == member.Id);

                if (memberEntity != null)
                {
                    memberEntity.NAME = member.Name;
                    memberEntity.IMAGE = member.Image;
                    memberEntity.PHONE = !string.IsNullOrEmpty(member.PhoneNumber)
                        ? Decimal.Parse(member.PhoneNumber)
                        : (decimal?)null;
                    memberEntity.REMARK = member.Remark;
                    memberEntity.UPDATE_DATE = DateTime.Now;

                    context.MEMBERs.Attach(memberEntity);
                    var entry = context.Entry(memberEntity);
                    entry.Property(x => x.NAME).IsModified = true;
                    entry.Property(x => x.IMAGE).IsModified = true;
                    entry.Property(x => x.PHONE).IsModified = true;
                    entry.Property(x => x.REMARK).IsModified = true;
                    entry.Property(x => x.UPDATE_DATE).IsModified = true;
                    context.SaveChanges();

                }
            }
        }

        public void Extend(Member member)
        {
            using (GYM_MANAGEREntities context = new GYM_MANAGEREntities())
            {
                var memberEntity = context.MEMBERs.FirstOrDefault(x => x.ID == member.Id);

                if (memberEntity != null)
                {
                    memberEntity.REGISTRATION_HISTORY = JsonConvert.SerializeObject(member.RegistrationHistory);
                    memberEntity.LAST_REGISTRATION = member.LastRegistrationDate;
                    memberEntity.LAST_REGISTRATION_MONEY = member.LastRegistrationMoney;
                    memberEntity.LAST_EXPIRED_DATE = member.LastExpiredDate;
                    memberEntity.UPDATE_DATE = DateTime.Now;

                    context.MEMBERs.Attach(memberEntity);
                    var entry = context.Entry(memberEntity);
                    entry.Property(x => x.REGISTRATION_HISTORY).IsModified = true;
                    entry.Property(x => x.LAST_REGISTRATION).IsModified = true;
                    entry.Property(x => x.LAST_REGISTRATION_MONEY).IsModified = true;
                    entry.Property(x => x.LAST_EXPIRED_DATE).IsModified = true;
                    entry.Property(x => x.UPDATE_DATE).IsModified = true;
                    context.SaveChanges();

                }
            }
        }

        public List<Member> Search(string input)
        {
            List<Member> members;

            if (string.IsNullOrEmpty(input))
            {
                using (GYM_MANAGEREntities context = new GYM_MANAGEREntities())
                {
                    IQueryable<Member> query = from m in context.MEMBERs
                                               where m.DELETED == false
                                               orderby m.LAST_REGISTRATION descending

                                               select new Member
                                               {
                                                   Id = m.ID,
                                                   Name = m.NAME,
                                                   Image = m.IMAGE,
                                                   PhoneNumber = m.PHONE.ToString(),
                                                   Remark = m.REMARK,
                                                   RegistrationHistoryString = m.REGISTRATION_HISTORY,
                                                   LastRegistrationDate = m.LAST_REGISTRATION.Value,
                                                   LastRegistrationMoney = m.LAST_REGISTRATION_MONEY.Value,
                                                   LastExpiredDate = m.LAST_EXPIRED_DATE.Value,
                                                   CreateDate = m.CREATE_DATE.Value,
                                                   UpdateDate = m.UPDATE_DATE
                                               };
                    members = query.Take(20).ToList();
                }
            }
            else
            {
                using (GYM_MANAGEREntities context = new GYM_MANAGEREntities())
                {
                    IQueryable<Member> query = from m in context.MEMBERs
                                               where m.DELETED == false && (m.PHONE.ToString().Contains(input) || m.NAME.Contains(input) || m.REMARK.Contains(input))
                                               orderby m.LAST_REGISTRATION descending

                                               select new Member
                                               {
                                                   Id = m.ID,
                                                   Name = m.NAME,
                                                   Image = m.IMAGE,
                                                   PhoneNumber = m.PHONE.ToString(),
                                                   Remark = m.REMARK,
                                                   RegistrationHistoryString = m.REGISTRATION_HISTORY,
                                                   LastRegistrationDate = m.LAST_REGISTRATION.Value,
                                                   LastRegistrationMoney = m.LAST_REGISTRATION_MONEY.Value,
                                                   LastExpiredDate = m.LAST_EXPIRED_DATE.Value,
                                                   CreateDate = m.CREATE_DATE.Value,
                                                   UpdateDate = m.UPDATE_DATE
                                               };
                    members = query.ToList();
                }
            }

            foreach (Member m in members)
            {
                m.RegistrationHistory = JsonConvert.DeserializeObject<List<Registration>>(m.RegistrationHistoryString);
            }

            return members;
        }

        public Member FindById(int id)
        {
            List<Member> members;

            using (GYM_MANAGEREntities context = new GYM_MANAGEREntities())
            {

                IQueryable<Member> query = from m in context.MEMBERs
                                           where m.ID == id
                                           select new Member
                                           {
                                               Id = m.ID,
                                               Name = m.NAME,
                                               Image = m.IMAGE,
                                               PhoneNumber = m.PHONE.ToString(),
                                               Remark = m.REMARK,
                                               RegistrationHistoryString = m.REGISTRATION_HISTORY,
                                               LastRegistrationDate = m.LAST_REGISTRATION.Value,
                                               LastRegistrationMoney = m.LAST_REGISTRATION_MONEY.Value,
                                               LastExpiredDate = m.LAST_EXPIRED_DATE.Value,
                                               CreateDate = m.CREATE_DATE.Value,
                                               UpdateDate = m.UPDATE_DATE
                                           };
                members = query.ToList();
            }

            var member = members.First();
            member.RegistrationHistory = JsonConvert.DeserializeObject<List<Registration>>(member.RegistrationHistoryString);

            return member;
        }
    }
}