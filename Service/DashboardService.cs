﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using gym_manager.Model;
using Newtonsoft.Json;

namespace gym_manager.Service
{
    public class DashboardService
    {
        public int GetNumberOfRegisterByMonth(int month)
        {
            using (GYM_MANAGEREntities context = new GYM_MANAGEREntities())
            {
                return context.MEMBERs.Count(x => x.LAST_REGISTRATION.Value.Month == month && x.DELETED == false);
            }
        }

        public Decimal GeMoneyByMonth(DateTime dateTime)
        {
            List<Member> members;
            Decimal total = 0;
            using (GYM_MANAGEREntities context = new GYM_MANAGEREntities())
            {

                IQueryable<Member> query = from m in context.MEMBERs
                                           where m.LAST_REGISTRATION >= dateTime.Date && m.DELETED == false
                                           select new Member
                                           {
                                               Id = m.ID,
                                               Name = m.NAME,
                                               Image = m.IMAGE,
                                               PhoneNumber = m.PHONE.ToString(),
                                               Remark = m.REMARK,
                                               RegistrationHistoryString = m.REGISTRATION_HISTORY,
                                               LastRegistrationDate = m.LAST_REGISTRATION.Value,
                                               LastRegistrationMoney = m.LAST_REGISTRATION_MONEY.Value,
                                               LastExpiredDate = m.LAST_EXPIRED_DATE.Value,
                                               CreateDate = m.CREATE_DATE.Value,
                                               UpdateDate = m.UPDATE_DATE
                                           };
                members = query.ToList();
            }

            foreach (Member m in members)
            {
                m.RegistrationHistory = JsonConvert.DeserializeObject<List<Registration>>(m.RegistrationHistoryString);

                foreach (Registration r in m.RegistrationHistory)
                {
                    if (r.RegistrationDate.Month == dateTime.Month)
                    {
                        total += r.Price;
                    }
                }
            }

            return total;
        }
    }
}
