﻿using System;
using System.Collections.Generic;

namespace gym_manager.Model
{
    public class Member
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public Byte[] Image { get; set; }
        public String PhoneNumber { get; set; }
        public String Remark { get; set; }
        public DateTime LastRegistrationDate { get; set; }
        public Decimal LastRegistrationMoney { get; set; }
        public DateTime LastExpiredDate { get; set; }
        public List<Registration> RegistrationHistory { get; set; }
        public string RegistrationHistoryString { get; set; }
        public String RegistrationHistoryDisplay
        {
            get
            {
                string result = string.Empty;

                foreach (Registration r in RegistrationHistory)
                {
                    result += r.RegistrationDate.ToString("dd/MM/yyyy") + ", Money: " + r.Price + "\n";
                }

                return result;
            }
            set => throw new NotImplementedException();
        }

        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }

    public class Registration
    {
        public Int64 Price { get; set; }
        public DateTime RegistrationDate { get; set; }
    }
}